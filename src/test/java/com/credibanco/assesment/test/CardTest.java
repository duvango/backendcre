package com.credibanco.assesment.test;

import com.assessment.card.dto.RequestGenerateCardDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.core.Is.is;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
class CardTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void canCreateCard()throws Exception{
        RequestGenerateCardDTO requestGenerateCardDTO=new  RequestGenerateCardDTO();
        requestGenerateCardDTO.setClient("Sebastian Torres");
        requestGenerateCardDTO.setPan(12345678902233L);
        requestGenerateCardDTO.setPhone(31144517L);
        requestGenerateCardDTO.setType("Credito");
        requestGenerateCardDTO.setIdentificationNumber(12346344L);
        MvcResult resultCreateCard= mvc.perform(MockMvcRequestBuilders.post("/api/v1/card").
                contentType(MediaType.APPLICATION_JSON).
                content(objectMapper.writeValueAsString(requestGenerateCardDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.code").exists())
                .andExpect(jsonPath("$.code").isString())
                .andReturn();
    }
}
