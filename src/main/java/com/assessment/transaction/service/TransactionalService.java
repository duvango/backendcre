package com.assessment.transaction.service;

import com.assessment.card.dto.ResponseFindCardDTO;
import com.assessment.card.model.Card;
import com.assessment.card.service.impl.CardServiceImpl;
import com.assessment.transaction.dto.*;
import com.assessment.transaction.model.Transaction;
import com.assessment.transaction.repository.TransactionRepository;
import com.assessment.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Service
public class TransactionalService {
    private final TransactionRepository transactionRepository;
    private final CardServiceImpl cardService;

    @Autowired
    public TransactionalService(TransactionRepository transactionRepository, CardServiceImpl cardService) {
        this.transactionRepository = transactionRepository;
        this.cardService = cardService;
    }


    public ResponseCreateTransactionDTO createTransaction(RequestCreateTransactionDTO requestCreateTransaction){
        Mapper mapper=new Mapper();
        Transaction transaction= mapper.mapperCreateTransactionEntity(requestCreateTransaction);
        ResponseCreateTransactionDTO responseCreateTransaction=new ResponseCreateTransactionDTO();
        responseCreateTransaction.setReferenceNumber(requestCreateTransaction.getReferenceNumber());
        String isValidateCard= validateData(transaction);
        LocalDateTime now= LocalDateTime.now();
        transaction.setDateTime(now);
        switch (isValidateCard){
            case "Enrolada":
                responseCreateTransaction.setCodeResponse("00");
                responseCreateTransaction.setMessageResponse("Compra exitosa");
                responseCreateTransaction.setStatus("Aprobada");
                transaction.setStatus("Aprobada");
                break;
            case "Tarjeta no existe":
                responseCreateTransaction.setCodeResponse("01");
                responseCreateTransaction.setMessageResponse(isValidateCard);
                responseCreateTransaction.setStatus("Rechazada");
                transaction.setStatus("Rechazada");
                break;
            case "Tarjeta no Enrolada":
                responseCreateTransaction.setCodeResponse("02");
                responseCreateTransaction.setMessageResponse(isValidateCard);
                responseCreateTransaction.setStatus("Rechazada");
                transaction.setStatus("Rechazada");
                break;
        }
        transactionRepository.save(transaction);

        return responseCreateTransaction;
    }

    private String validateData(Transaction transaction) {
        String isValidateCard="Tarjeta no existe";
        try {
            ResponseFindCardDTO card= cardService.findCard(transaction.getIdentificationSystem());
            if(card.getStatus().equals("Enrolada")){
                isValidateCard="Enrolada";
            }else {
                isValidateCard="Tarjeta no Enrolada";
            }
        }catch (Exception e){
        }
        return isValidateCard;
    }

    public ResponseCancelTransactionDTO cancelTransaction(RequestCancelTransactionDTO requestCancelTransaction){
        ResponseCancelTransactionDTO responseCancelTransaction=new ResponseCancelTransactionDTO();
        List<Transaction> transactions=transactionRepository.findAllByidentificationSystem(requestCancelTransaction.getIdentificationSystem());
        if(transactions.stream().count()>0){
            responseCancelTransaction.setReferenceNumber(requestCancelTransaction.getReferenceNumber());
            String canCancelTransaction= validateIfCanTransaction(transactions,requestCancelTransaction);
            if(canCancelTransaction=="Existe"){
                responseCancelTransaction.setCodeResponse("00");
                responseCancelTransaction.setMessageResponse("Compra anulada");
            }
            else if (canCancelTransaction=="Número de referencia invalido"){
                responseCancelTransaction.setCodeResponse("01");
                responseCancelTransaction.setMessageResponse(canCancelTransaction);
            }else {
                responseCancelTransaction.setCodeResponse("02");
                responseCancelTransaction.setMessageResponse("No se puede anular transacción");
            }
        }
        return responseCancelTransaction;
    }

    private String validateIfCanTransaction(List<Transaction> transactions, RequestCancelTransactionDTO requestCancelTransaction) {
        String statusValidateTransaction="";

            for (Transaction transaction:transactions) {
                if(transaction.getAmount().equals(requestCancelTransaction.getAmount()) && transaction.getReferenceNumber().equals(requestCancelTransaction.getReferenceNumber())){
                    LocalDateTime now=LocalDateTime.now();
                    LocalDateTime transactionDate=transaction.getDateTime();
                    Duration duration = Duration.between(now, transactionDate);
                    long diff = Math.abs(duration.toMinutes());
                    if(diff<5){
                        statusValidateTransaction="Existe";
                    }

                }else {
                    statusValidateTransaction="Número de referencia invalido";
                }

            }

        return  statusValidateTransaction;
    }

    public List<ResponseAllTransactionDTO> getAllTransaction() {
        Mapper mapper=new Mapper();
        List<Transaction> transactions=transactionRepository.findAll();
        List<ResponseAllTransactionDTO> responseAllTransactions=mapper.mapperTransactionsToResponseAllTransactionDTO(transactions);
        return responseAllTransactions;
    }
}
