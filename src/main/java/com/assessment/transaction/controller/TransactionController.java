package com.assessment.transaction.controller;

import com.assessment.transaction.dto.*;
import com.assessment.transaction.service.TransactionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/transaction")
public class TransactionController {
    private final TransactionalService transactionalService;

    @Autowired
    public TransactionController(TransactionalService transactionalService) {
        this.transactionalService = transactionalService;
    }

    @GetMapping
    public List<ResponseAllTransactionDTO> getAllTransaction(){
        return transactionalService.getAllTransaction();
    }

    @PostMapping
    public ResponseCreateTransactionDTO createTransaction(@RequestBody RequestCreateTransactionDTO requestCreateTransaction){
        return transactionalService.createTransaction(requestCreateTransaction);
    }

    @PutMapping
    public ResponseCancelTransactionDTO cancelTransaction(@RequestBody RequestCancelTransactionDTO requestCancelTransaction){
        return transactionalService.cancelTransaction(requestCancelTransaction);
    }
}
