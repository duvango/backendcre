package com.assessment.transaction.dto;

public class RequestCreateTransactionDTO {
    String identificationSystem;
    Long referenceNumber;
    Double Amount;
    String Address;

    public String getIdentificationSystem() {
        return identificationSystem;
    }

    public void setIdentificationSystem(String identificationSystem) {
        this.identificationSystem = identificationSystem;
    }

    public Long getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(Long referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public Double getAmount() {
        return Amount;
    }

    public void setAmount(Double amount) {
        Amount = amount;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }
}
