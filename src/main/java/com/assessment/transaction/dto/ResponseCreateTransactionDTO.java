package com.assessment.transaction.dto;

public class ResponseCreateTransactionDTO extends  ResponseTransactionDTO{
    String status;
    Long referenceNumber;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(Long referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
}
