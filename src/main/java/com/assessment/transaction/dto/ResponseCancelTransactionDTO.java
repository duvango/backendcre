package com.assessment.transaction.dto;

public class ResponseCancelTransactionDTO extends ResponseTransactionDTO{
    Long referenceNumber;

    public Long getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(Long referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
}
