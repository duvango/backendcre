package com.assessment.transaction.repository;

import com.assessment.transaction.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction,Integer> {
    List<Transaction> findAllByidentificationSystem(String identificationSystem);
}
