package com.assessment.util;

import com.assessment.card.dto.RequestGenerateCardDTO;
import com.assessment.card.dto.ResponseEnrollCardDTO;
import com.assessment.card.dto.ResponseFindCardDTO;
import com.assessment.card.dto.ResponseGenerateCardDTO;
import com.assessment.card.model.Card;
import com.assessment.transaction.dto.RequestCreateTransactionDTO;
import com.assessment.transaction.dto.ResponseAllTransactionDTO;
import com.assessment.transaction.model.Transaction;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class Mapper {

    public Card mapperCreateDTOToEntity(RequestGenerateCardDTO requestGenerateCardDTO)  {
        Card card=new Card();
        try {

            card.setPan(requestGenerateCardDTO.getPan());
            card.setPhone(requestGenerateCardDTO.getPhone());
            card.setIdentificationSystem(Generators.generateIdentificationSystemNumber(requestGenerateCardDTO.getPan()));
            card.setValidationNumber(Generators.validationNumber());
            card.setPersonalIdentification(requestGenerateCardDTO.getIdentificationNumber());
            card.setCardHolder(requestGenerateCardDTO.getClient());
            card.setStatus("Creada");

        }catch (Exception e){

        }
        return card;
    }
    public ResponseGenerateCardDTO mapperCreateEntityToDTO(Card card){
        ResponseGenerateCardDTO responseGenerateCardDTO = new ResponseGenerateCardDTO();
        responseGenerateCardDTO.setValidationNumber(card.getValidationNumber());
        responseGenerateCardDTO.setIdentificationSystem(card.getIdentificationSystem());
        responseGenerateCardDTO.setPan(Encrypt.generetePanEncrypt(card.getPan()));
        return responseGenerateCardDTO;
    }
    public ResponseFindCardDTO mapperFindEntityToDTO(Card card){
        ResponseFindCardDTO responseFindCardDTO=new ResponseFindCardDTO();
        responseFindCardDTO.setPan(Encrypt.generetePanEncrypt(card.getPan()));
        responseFindCardDTO.setCardHolder(card.getCardHolder());
        responseFindCardDTO.setPersonalIdentification(card.getPersonalIdentification());
        responseFindCardDTO.setPhone(card.getPhone());
        responseFindCardDTO.setStatus(card.getStatus());
        return responseFindCardDTO;
    }

    public Transaction mapperCreateTransactionEntity(RequestCreateTransactionDTO requestCreateTransaction) {
        Transaction transaction=new Transaction();
        transaction.setIdentificationSystem(requestCreateTransaction.getIdentificationSystem());
        transaction.setReferenceNumber(requestCreateTransaction.getReferenceNumber());
        transaction.setAmount(requestCreateTransaction.getAmount());
        transaction.setAddress(requestCreateTransaction.getAddress());
        return transaction;
    }

    public List<ResponseFindCardDTO> mapperCardsToResponseFindCardDTO(List<Card> cards) {
        List<ResponseFindCardDTO> responseFindCards=new ArrayList<ResponseFindCardDTO>();
        for (Card card:cards) {
            ResponseFindCardDTO responseFindCard= new ResponseFindCardDTO();
            responseFindCard.setPan(Encrypt.generetePanEncrypt(card.getPan()));
            responseFindCard.setCardHolder(card.getCardHolder());
            responseFindCard.setStatus(card.getStatus());
            responseFindCard.setPersonalIdentification(card.getPersonalIdentification());
            responseFindCard.setPhone(card.getPhone());
            responseFindCards.add(responseFindCard);
        }

        return responseFindCards;
    }

    public List<ResponseAllTransactionDTO> mapperTransactionsToResponseAllTransactionDTO(List<Transaction> transactions) {
        List<ResponseAllTransactionDTO> responseAllTransactions= new ArrayList<ResponseAllTransactionDTO>();
        for(Transaction transaction:transactions){
            ResponseAllTransactionDTO responseAllTransaction=new ResponseAllTransactionDTO();
            responseAllTransaction.setId(transaction.getId());
            responseAllTransaction.setAddress(transaction.getAddress());
            responseAllTransaction.setAmount(transaction.getAmount());
            responseAllTransaction.setStatus(transaction.getStatus());
            responseAllTransaction.setIdentificationSystem(transaction.getIdentificationSystem());
            responseAllTransaction.setReferenceNumber(transaction.getReferenceNumber());
            responseAllTransaction.setDateTime(transaction.getDateTime());
            responseAllTransactions.add(responseAllTransaction);
        }
        return responseAllTransactions;
    }
}
