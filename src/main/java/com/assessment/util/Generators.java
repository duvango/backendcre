package com.assessment.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Generators {

    public static String  generateIdentificationSystemNumber(Long pan) throws NoSuchAlgorithmException {
        String inputPan = pan.toString();
        String hashtext = null;
        MessageDigest md = MessageDigest.getInstance("MD5");

        // Compute message digest of the input
        byte[] messageDigest = md.digest(inputPan.getBytes());

        hashtext = convertToHex(messageDigest);

        return hashtext;
    }

    private static String convertToHex(final byte[] messageDigest) {
        BigInteger bigint = new BigInteger(1, messageDigest);
        String hexText = bigint.toString(16);
        while (hexText.length() < 32) {
            hexText = "0".concat(hexText);
        }
        return hexText;
    }

    public static Integer validationNumber(){
        return (int)(Math.random() * (100 - 1 + 1) + 1);
    }
}
