package com.assessment.card.service.impl;

import com.assessment.card.dto.*;
import com.assessment.card.exceptions.FieldRequestCard;
import com.assessment.card.exceptions.NotFoundCard;
import com.assessment.card.model.Card;
import com.assessment.card.repository.CardRepository;
import com.assessment.card.service.ICardService;
import com.assessment.util.Encrypt;
import com.assessment.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardServiceImpl implements ICardService {
    private final CardRepository cardRepository;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    public ResponseGenerateCardDTO createCard(RequestGenerateCardDTO requestGenerateCardDTO){
        boolean isValid = validateRequest(requestGenerateCardDTO);
        Card card=new Card();
        Mapper mapper=new Mapper();
        card= mapper.mapperCreateDTOToEntity(requestGenerateCardDTO);
        cardRepository.save(card);
        ResponseGenerateCardDTO responseGenerateCardDTO=new ResponseGenerateCardDTO();
        responseGenerateCardDTO=mapper.mapperCreateEntityToDTO(card);
        responseGenerateCardDTO.setCode("001");
        responseGenerateCardDTO.setMessage("Exito");
        return responseGenerateCardDTO;
    }

    private boolean validateRequest(RequestGenerateCardDTO requestGenerateCardDTO) {
        boolean isValid=true;
        if(requestGenerateCardDTO.getPan().toString().length()!=14){
            isValid=false;
            throw new FieldRequestCard("La tarjeta debe contener 14 caracteres numericos");
        }
        return isValid;
    }

    public List<ResponseFindCardDTO> getAllCard(){
       Mapper mapper=new Mapper();
       List<Card> cards=cardRepository.findAll();
       List<ResponseFindCardDTO> responseFindCards=mapper.mapperCardsToResponseFindCardDTO(cards);

       return responseFindCards;
    }

    public ResponseEnrollCardDTO enrollCard(RequestEnrollCardDTO requestEnrollCardDTO){
        Mapper mapper=new Mapper();
        ResponseEnrollCardDTO responseEnrollCardDTO=new ResponseEnrollCardDTO();
        //Card card=new Card();
        Card card= cardRepository.findByidentificationSystem(requestEnrollCardDTO.getIdentificationSystem());
        if(card!=null){
            boolean isCorrectValidation = isNumberValidation(card,requestEnrollCardDTO.getValidationNumber());
            if(isCorrectValidation){
                card.setStatus("Enrolada");
                cardRepository.save(card);
                responseEnrollCardDTO.setCode("00");
                responseEnrollCardDTO.setMessage("Éxito");
                responseEnrollCardDTO.setPan(Encrypt.generetePanEncrypt(card.getPan()));
            }else{
                responseEnrollCardDTO.setCode("01");
                responseEnrollCardDTO.setMessage("Número de validación inválido");
            }
        }else{
            responseEnrollCardDTO.setCode("01");
            responseEnrollCardDTO.setMessage("Tarjeta no existe");
        }

        return responseEnrollCardDTO;
    }

    public ResponseFindCardDTO findCard(String indetificationSystem) {

        Mapper mapper=new Mapper();
        ResponseFindCardDTO responseFindCardDTO=new ResponseFindCardDTO();

            Card card=cardRepository.findByidentificationSystem(indetificationSystem);
            if(card!=null){
                responseFindCardDTO=mapper.mapperFindEntityToDTO(card);
            }else{
                throw  new NotFoundCard("Tarjeta no encontrada");
            }




        return responseFindCardDTO;
    }

    public ResponseDTO deleteCard(RequestDeleteCardDTO requestDeleteCardDTO) {
        ResponseDTO responseDTO=new ResponseDTO();
        Card card=cardRepository.findByidentificationSystem(requestDeleteCardDTO.getIdentificationSystem());
        if(card!=null){
            boolean isCorrectValidation = isNumberValidation(card,requestDeleteCardDTO.getValidationNumber());
            if(isCorrectValidation){
                if(card.getPan().equals(requestDeleteCardDTO.getPan())){
                    cardRepository.delete(card);
                    responseDTO.setCode("00");
                    responseDTO.setMessage("Se ha eliminado la tarjeta");
                }else{
                    responseDTO.setCode("01");
                    responseDTO.setMessage("No se ha eliminado la tarjeta");
                }
            }else{
                responseDTO.setCode("01");
                responseDTO.setMessage("No se ha eliminado la tarjeta");
            }
        }
        return responseDTO;
    }

    private boolean isNumberValidation(Card card, Integer validationNumber) {
        boolean isCard=false;
        if(card.getValidationNumber()==validationNumber){
            isCard=true;
        }
        return isCard;
    }
}
