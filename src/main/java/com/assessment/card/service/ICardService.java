package com.assessment.card.service;

import com.assessment.card.dto.*;

import java.util.List;

public interface ICardService {
    public ResponseGenerateCardDTO createCard(RequestGenerateCardDTO requestGenerateCardDTO);
    public ResponseEnrollCardDTO enrollCard(RequestEnrollCardDTO requestEnrollCardDTO);
    public ResponseFindCardDTO findCard(String indetificationSystem);
    public ResponseDTO deleteCard(RequestDeleteCardDTO requestDeleteCardDTO);
    public List<ResponseFindCardDTO> getAllCard();
}
