package com.assessment.card.dto;

public class ResponseEnrollCardDTO extends ResponseDTO {
    String pan;

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }
}
