package com.assessment.card.dto;

import org.springframework.data.relational.core.sql.In;

public class ResponseGenerateCardDTO extends ResponseDTO {
    Integer validationNumber;
    String pan;
    String identificationSystem;

    public Integer getValidationNumber() {
        return validationNumber;
    }

    public void setValidationNumber(Integer validationNumber) {
        this.validationNumber = validationNumber;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getIdentificationSystem() {
        return identificationSystem;
    }

    public void setIdentificationSystem(String identificationSystem) {
        this.identificationSystem = identificationSystem;
    }
}
