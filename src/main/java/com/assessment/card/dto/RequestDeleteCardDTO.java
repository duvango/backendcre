package com.assessment.card.dto;

public class RequestDeleteCardDTO {
    String identificationSystem;
    Integer validationNumber;
    Long pan;

    public String getIdentificationSystem() {
        return identificationSystem;
    }

    public void setIdentificationSystem(String identificationSystem) {
        this.identificationSystem = identificationSystem;
    }

    public Integer getValidationNumber() {
        return validationNumber;
    }

    public void setValidationNumber(Integer validationNumber) {
        this.validationNumber = validationNumber;
    }

    public Long getPan() {
        return pan;
    }

    public void setPan(Long pan) {
        this.pan = pan;
    }
}
