package com.assessment.card.dto;

public class RequestEnrollCardDTO {
    Integer validationNumber;
    String identificationSystem;

    public Integer getValidationNumber() {
        return validationNumber;
    }

    public void setValidationNumber(Integer validationNumber) {
        this.validationNumber = validationNumber;
    }

    public String getIdentificationSystem() {
        return identificationSystem;
    }

    public void setIdentificationSystem(String identificationSystem) {
        this.identificationSystem = identificationSystem;
    }
}
