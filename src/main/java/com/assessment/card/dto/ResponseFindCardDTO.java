package com.assessment.card.dto;

public class ResponseFindCardDTO{
    String pan;
    String cardHolder;
    Long personalIdentification;
    Long phone;
    String status;

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public Long getPersonalIdentification() {
        return personalIdentification;
    }

    public void setPersonalIdentification(Long personalIdentification) {
        this.personalIdentification = personalIdentification;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
