package com.assessment.card.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    Integer validationNumber;
    Long pan;
    String status;
    Long phone;
    Long personalIdentification;
    String cardHolder;
    String identificationSystem;

    public Card( Integer validationNumber, Long pan, String status, Long phone, Long personalIdentification, String cardHolder, String identificationSystem) {

        this.validationNumber = validationNumber;
        this.pan = pan;
        this.status = status;
        this.phone = phone;
        this.personalIdentification = personalIdentification;
        this.cardHolder = cardHolder;
        this.identificationSystem = identificationSystem;
    }

    public Card() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getValidationNumber() {
        return validationNumber;
    }

    public void setValidationNumber(Integer validationNumber) {
        this.validationNumber = validationNumber;
    }

    public Long getPan() {
        return pan;
    }

    public void setPan(Long pan) {
        this.pan = pan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public Long getPersonalIdentification() {
        return personalIdentification;
    }

    public void setPersonalIdentification(Long personalIdentification) {
        this.personalIdentification = personalIdentification;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getIdentificationSystem() {
        return identificationSystem;
    }

    public void setIdentificationSystem(String identificationSystem) {
        this.identificationSystem = identificationSystem;
    }
}
