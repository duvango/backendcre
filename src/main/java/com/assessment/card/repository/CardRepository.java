package com.assessment.card.repository;

import com.assessment.card.model.Card;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface CardRepository extends JpaRepository<Card, Integer> {
         Card findByidentificationSystem(String identificationSystem);
}
