package com.assessment.card.controller;

import com.assessment.card.dto.*;
import com.assessment.card.service.ICardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/card")
public class CardController {
    private final ICardService serviceCard;

    @Autowired
    public CardController(ICardService serviceCard) {
        this.serviceCard = serviceCard;
    }

    @GetMapping("/{identificationSystem}")
    public ResponseFindCardDTO getCard(@PathVariable String identificationSystem){
        return serviceCard.findCard(identificationSystem);
    }
    @GetMapping
    public List<ResponseFindCardDTO> getAllCard(){
        return serviceCard.getAllCard();
    }
    @PostMapping
    public ResponseDTO createCard(@RequestBody RequestGenerateCardDTO requestDTO){
        return serviceCard.createCard(requestDTO);
    }
    @PostMapping(value = "/enroll")
    public ResponseDTO createCard(@RequestBody RequestEnrollCardDTO requestEnrollCardDTO){
      return serviceCard.enrollCard(requestEnrollCardDTO);
    }
    @DeleteMapping
    public ResponseDTO deleteCard(@RequestBody RequestDeleteCardDTO requestDeleteCardDTO){
        return serviceCard.deleteCard(requestDeleteCardDTO);
    }

}
