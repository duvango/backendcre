package com.assessment.card.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class FieldRequestCard extends RuntimeException{
    public FieldRequestCard(String message){
        super(message);
    }
}
