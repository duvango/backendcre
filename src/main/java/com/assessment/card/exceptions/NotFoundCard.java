package com.assessment.card.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundCard extends RuntimeException {
    public NotFoundCard(String message){
        super(message);
    }
}
