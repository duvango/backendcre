package com.assessment;

import com.assessment.util.Encrypt;
import com.assessment.util.Generators;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.NoSuchAlgorithmException;

@SpringBootApplication
public class Assessment {
    public static void main(String[] args) throws NoSuchAlgorithmException {

        SpringApplication.run(Assessment.class,args);

    }
}
